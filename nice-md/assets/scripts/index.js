let editor = null
editor = new Vditor('vditor', {
    mode: 'sv',
    height: 850,
    minHeight: 800,
    typewriterMode: true,
    placeholder: '请输入文章内容',
    outline: true,
    cache: {
        enable: true,
        id: '__vditor_content'
    },
    counter: {
        enable: true,
    },
    preview: {
        delay: 200,
        markdown: {
            autoSpace: true,
            theme: 'wechat',
            chinesePunct: true,
        },
        hljs: {
            enable: true,
            lineNumber: true,
            style: "monokai",
        },
        maxWidth: 800
    },
    hint: {
        emoji: {
            'doge': 'https://cdn.jsdelivr.net/npm/vditor@3.2.12/dist/images/emoji/doge.png',
            'wulian': 'https://cdn.jsdelivr.net/npm/vditor@3.2.12/dist/images/emoji/wulian.png',
            'huaji': 'https://cdn.jsdelivr.net/npm/vditor@3.2.12/dist/images/emoji/huaji.gif'
        },
        emojiPath: 'https://cdn.jsdelivr.net/npm/vditor@3.2.12/dist/images/emoji/',
    },
    toolbar: [
        "emoji",
        "headings",
        "bold",
        "italic",
        "strike",
        "link",
        "|",
        "list",
        "ordered-list",
        "check",
        "outdent",
        "indent",
        "|",
        "quote",
        "line",
        "code",
        "inline-code",
        "insert-before",
        "insert-after",
        "|",
        "upload",
        "table",
        "|",
        "undo",
        "redo",
        "|",
        "fullscreen",
        "edit-mode",
        {
            name: "more",
            toolbar: [
                "both",
                "code-theme",
                "content-theme",
                "export",
                "outline",
                "preview",
                "format",
                "help",
            ],
        }
    ],
    upload: {
        headers: "{'Content-Type': 'multipart/form-data'}",
        // headers: "{'Content-Type': 'application/json; charset=utf-8'}",
        // headers: "{'Access-Control-Allow-Origin': '*'}",
        // headers: "{'Content-Type': 'application/x-www-form-urlencoded'}",
        accept: ".jpg,.jpeg,.png,.gif",
        withCredentials: true,
        handler(files) {
            var data = {
                "msg": "",
                "code": 0,
                "data": {
                    "errFiles": [],
                    "succMap": {}
                }
            };
            let formData = new FormData();
            let xhr = new XMLHttpRequest();
            let tmp = '';
            var obj = '';
            url = 'https://imgkr.com/api/files/upload';
            for (var i in files) {
                formData.append('file', files[i]);
                xhr.open("post", url, true);
                xhr.onload = function() {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        console.log("上传成功");
                        obj = JSON.parse(xhr.responseText);
                        data.data.succMap[files[i].name] = obj.data;
                        tmp = '![' + files[i].name + '](' + obj.data + ')';
                        editor.insertValue(tmp);
                    } else {
                        data.data.errFiles = files[i].name;
                    }
                }
            }

            // url = 'https://v1.alapi.cn/api/image?type=Ali';
            // for (var i in files) {
            //     formData.append('image', files[i]);
            //     xhr.open("post", url, true);
            //     xhr.onload = function() {
            //         if (xhr.readyState === 4 && xhr.status === 200) {
            //             console.log("上传成功");
            //             obj = JSON.parse(xhr.responseText);
            //             data.data.succMap[files[i].name] = obj.data.url.Ali;
            //             tmp = '![' + files[i].name + '](' + obj.data.url.Ali + ')';
            //             editor.insertValue(tmp);
            //         } else {
            //             data.data.errFiles = files[i].name;
            //         }
            //     }
            // }

            // url = 'https://api.uomg.com/api/image.ali';
            // for (var i in files) {
            //     formData.append('file', 'multipart');
            //     formData.append('Filedata', files[i]);
            //     xhr.open("post", url, true);
            //     xhr.onload = function() {
            //         if (xhr.readyState === 4 && xhr.status === 200) {
            //             console.log("上传成功");
            //             obj = JSON.parse(xhr.responseText);
            //             data.data.succMap[files[i].name] = obj.imgurl;
            //             tmp = '![' + files[i].name + '](' + obj.imgurl + ')';
            //             editor.insertValue(tmp);
            //         } else {
            //             data.data.errFiles = files[i].name;
            //         }
            //     }
            // }
            xhr.send(formData);
            console.log(data);
            return data;
        },

    },
});